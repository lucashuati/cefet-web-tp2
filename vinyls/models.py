from django.db import models

class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    date_birth = models.DateField()
    RULE = (
        ('M', 'Musician'),
        ('E', 'Entrepreneur')
    )
    rule = models.CharField(max_length=1, choices=RULE)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Musician(models.Model) :
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    style = models.CharField(max_length=30)

    def __str__(self):
        return self.person

class Band(models.Model):
    name = models.CharField(max_length=30)
    musicians = models.ManyToManyField(Musician)
    on_action = models.BooleanField(default=True)
    sytle = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class RecordCompany(models.Model):
    owner = models.ForeignKey(Person, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Vinyl(models.Model):
    record_company = models.ForeignKey(RecordCompany, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    band = models.ForeignKey(Band, on_delete=models.CASCADE)
    description = models.CharField(max_length=300)
    cover = models.ImageField(null=True, blank=True)

    def __str__(self):
        return f'{self.band}: {self.name}'
