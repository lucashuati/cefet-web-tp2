from django.forms import ModelForm


class VinylForm(ModelForm):
    class meta:
        model = Vinyl
        fields = ['record_company', 'name', 'band', 'cover', 'description']
