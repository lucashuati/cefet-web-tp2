from django.shortcuts import render, redirect, get_object_or_404
from .models import Vinyl
from django.views.generic.list import ListView
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.csrf import csrf_exempt


class VinylSaveView(LoginRequiredMixin):
    model = Vinyl
    fields = ['record_company', 'name', 'band', 'cover', 'description']
    template_name = 'vinyls/vinyl_form.html'
    success_url = reverse_lazy('list')


class VinylCreateView(VinylSaveView, CreateView):
    pass


class VinylUpdateView(VinylSaveView, UpdateView):
    pass


@method_decorator(cache_page(60*10), name='dispatch')
class VinylListView(LoginRequiredMixin, ListView):
    model = Vinyl
    template_name = 'vinyls/vinyl_list.html'


@method_decorator(csrf_exempt, name='dispatch')
class VinylDeleleView(LoginRequiredMixin, DeleteView):
    model = Vinyl
    success_url = reverse_lazy('list')
