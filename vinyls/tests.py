from django.test import TestCase, Client
from . import models


class TestingUtils:
    @staticmethod
    def login(client):
        return client.post('/login/', {'username': 'testing', 'password': '131313'})


class LoginTestCase(TestCase):
    fixtures = ['initial_user.json']

    def test_login_success(self):
        client = Client()
        TestingUtils.login(client)
        response = client.get('/vinyls/')
        self.assertEqual(response.status_code, 200)


class CreateVinylTestCase(TestCase):
    fixtures = ['initial_user.json', 'initial_data.json']

    def setUp(self):
        self.client = Client()
        TestingUtils.login(self.client)


    def test_create_vinyl_success(self):

        response = self.client.post(
            '/vinyls/new',
            {
                'record_company': 1,
                'name': 'Testando',
                'band': 1,
                'description': 'description'
            }
        )
        self.assertRedirects(response, '/vinyls/')


class UpdateVinylTestCase(TestCase):
    fixtures = ['initial_user.json', 'initial_data.json']

    def setUp(self):
        self.client = Client()
        TestingUtils.login(self.client)


    def test_create_vinyl_success(self):
        vinyl = models.Vinyl.objects.create(
            record_company_id=1,
            name='Testando',
            band_id=1,
            description='description'
        )
        response = self.client.post(
            f'/vinyls/edit/{vinyl.pk}',
            {
                'record_company': 1,
                'name': 'New Name',
                'band': 1,
                'description': 'description'
            }
        )
        self.assertRedirects(response, '/vinyls/')


class DeleteVinylTestCase(TestCase):
    fixtures = ['initial_user.json', 'initial_data.json']

    def setUp(self):
        self.client = Client()
        TestingUtils.login(self.client)


    def test_create_vinyl_success(self):
        vinyl = models.Vinyl.objects.create(
            record_company_id=1,
            name='Testando',
            band_id=1,
            description='description'
        )
        response = self.client.post(
            f'/vinyls/delete/{vinyl.pk}', {}
        )
        self.assertRedirects(response, '/vinyls/')

