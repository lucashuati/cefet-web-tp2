from django.urls import path
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.VinylListView.as_view(), name='list'),
    path('new', views.VinylCreateView.as_view(), name='create'),
    path('edit/<int:pk>', views.VinylUpdateView.as_view(), name='update'),
    path('delete/<int:pk>', views.VinylDeleleView.as_view(), name='delete'),

]
