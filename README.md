# Trabalho Prático 2 - Django Livre

Esse projeto é uma aplicação CRUD de Django, que insere Vinis no sistema. Sendo que um objeto Vinil é composto de tal forma que:

```
Vinil
    Gravadora
    Artista
    Nome
    Descrição
    Estilo Musical
    Produtor
    Arte
```

## Deployment

Add additional notes about how to deploy this on a live system

## Instituição e Curso de Desenvolvimento Web

* [Cefet](http://cefetmg.br) - Cefet-MG Website
* [Curso](daniel-hasan.github.io/cefet-web-grad) - Cefet Web Grad

## Autores

* **Barbara Poliana Jaber** - [PurpleBooth](https://github.com/bpoliana)
* **Lucas Correa Huati**  - [PurpleBooth](https://github.com/lucashuati)
* **Mauro Floriano**  - [PurpleBooth](https://github.com/maurofloriano)

## Professor

* **Daniel Hasan**  - [PurpleBooth](https://github.com/daniel-hasan)
